#include "myfs.h"
#include <string.h>
#include <iostream>
#include <math.h>
#include <sstream>

// Define the magic string for identifying the file system
const char *MyFs::MYFS_MAGIC = "MYFS";

// Static variables to keep track of current positions in the block device
static int current_place=0,inode_place =0;

// Constant for the size of a file
const int SIZE_FILE= 1000;

// Constructor for MyFs class
MyFs::MyFs(BlockDeviceSimulator *blkdevsim_):blkdevsim(blkdevsim_) {
    // Read the header from the block device
	struct myfs_header header;
	blkdevsim->read(0, sizeof(header), (char *)&header);

    // Check if the magic string matches and version is correct, if not, format the file system
	if (strncmp(header.magic, MYFS_MAGIC, sizeof(header.magic)) != 0 ||
	    (header.version != CURR_VERSION)) {
		std::cout << "Did not find myfs instance on blkdev" << std::endl;
		std::cout << "Creating..." << std::endl;
		format();
		std::cout << "Finished!" << std::endl;
	}
}

// Format the file system
void MyFs::format() {
    // Put the header in place
   	struct myfs_header header;
	strncpy(header.magic, MYFS_MAGIC, sizeof(header.magic));
	header.version = CURR_VERSION;
	blkdevsim->write(0, sizeof(header), (const char*)&header);
    current_place += sizeof(header);

    // Write inode_list to the block device
    size_t num_inodes = SIZE_FILE; // Adjust as needed
    size_t inode_list_size = sizeof(inoude_list_entry) * num_inodes;
    inode_place = current_place;
    current_place += inode_list_size;
}

// Create a string with dot content for file initialization
std::string MyFs::createDumpContent()
{
    std::string content=" ";
    int i=0;
    for(i=0;i<SIZE_FILE;i++)
    {
        content+='.';
    }
    return content;
}

// Create a file or directory in the file system
void MyFs::create_file(std::string path_str, bool directory) {
    // Check if file name is too long or directory is specified
   if (path_str.size() > SIZE_FILE || directory) 
   {
        throw std::runtime_error("File name too long or directory specified.");
    } else 
    {
        // Check if file name already exists
        for(const inoude_list_entry& file:this->InodeList)
        {
            if(file.file_data.name==path_str)
            {
                throw std::runtime_error("File name exists");
            }
        }
        
        // Create content for the file
        std::string content=createDumpContent();

        // Initialize file data
        dir_list_entry file_data;
        inoude_list_entry file;
        file_data.name = path_str;
        file_data.is_dir = directory;
        file_data.file_size = 0; // Initialize file size to 0 for new file
        file.file_data = file_data;

        // Assign inode number
        if (InodeList.empty()) {
            file.i = 1;
        } else 
        {
            file.i = InodeList.back().i + 1; // Increment last inode number
        }

        // Set the starting place for file content
        file.start_place = current_place;

        // Add file to the inode list
        InodeList.push_back(file);

        // Write file name to block device
        blkdevsim->write(current_place, path_str.length(), path_str.c_str());
        current_place += path_str.length();

        // Write file content to block device
        blkdevsim->write(current_place, SIZE_FILE, content.c_str());
        current_place += SIZE_FILE;

        // Write file data to the block device
        blkdevsim->write(inode_place, sizeof(inoude_list_entry), (const char *)(&file));
        inode_place += sizeof(inoude_list_entry);
    }	
}

// Get content of a file
std::string MyFs::get_content(std::string path_str) {
	throw std::runtime_error("not implemented");
	return "";
}

// Set content of a file
void MyFs::set_content(std::string path_str, std::string content) {
	throw std::runtime_error("not implemented");
}

// List contents of a directory
MyFs::dir_list MyFs::list_dir(std::string path_str) 
{
	dir_list ans;
	for(const inoude_list_entry & file:this->InodeList)
	{
		ans.push_back(file.file_data);
	}
	return ans;
}
